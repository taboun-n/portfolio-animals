# Animals

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
This is a simple project to learn Rails.

## Technologies
Project is created with:
* Rails: 6.0.3.2

## Setup
To run this project:
```
$
```

## Inspiration
https://www.planete-crocodiles.com/

## Contact
Created by [Nicolas Tabountchikoff](https://taboun-n.gitlab.io) - feel free to contact me!