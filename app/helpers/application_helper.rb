require "erb"

module ApplicationHelper

  def active_path?(path)
    request.path == path ? 'active' : ''
  end

end
